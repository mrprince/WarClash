package api.service;

import api.controller.BuildingController;
import api.implementation.BuildingServiceImpl;
import api.info.BuildInfo;
import authentication.JWTRESTDecorator;
import engine.Engine;
import engine.math.Vector3;
import model.WarClashGameTaskInfo;
import model.entity.BuildingData;
import model.entity.UnitData;
import model.repository.BuildingRepository;
import model.repository.CrudRepository;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.junit.jupiter.api.Test;
import rest.JsonReader;
import rest.RESTServer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BuildingServiceTest {
    private UUID gameId;

    @Test
    public void buildingServiceTest() throws IOException {
        RESTServer restServer = new RESTServer(8000);

        restServer.start();

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("warclash");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        var engine = new Engine<WarClashGameTaskInfo>();

        var is = BuildingServiceTest.class.getClassLoader().getResourceAsStream("test_1.map");
        List<Character> map = new ArrayList<>();
        if (is != null) {
            var reader = new InputStreamReader(is);
            int c;
            while ((c = reader.read()) != -1) {
                if (c != '\r') {
                    map.add((char)c);
                }
            }
        }

        engine.createGame(task -> {
            gameId = task.getGameId();
            return new WarClashGameTaskInfo(task, map);
        });

        restServer.addEndPoint("/building", new JWTRESTDecorator(new BuildingController(
                new BuildingServiceImpl(
                        engine,
                        new BuildingRepository(entityManager)
                ))));

        entityManager.getTransaction().begin();

        UnitData unitData = new UnitData();
        unitData.setNameKey("seryoga-zdanie");
        entityManager.persist(unitData);

        BuildingData buildingData = new BuildingData();
        buildingData.setNameKey("matya_zdanie");
        buildingData.setUnitData(unitData);
        entityManager.persist(buildingData);

        entityManager.getTransaction().commit();

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .header("Authorization", JWT.create().withSubject("warclash").withKeyId("111")
                        .sign(Algorithm.HMAC256(JWTRESTDecorator.SECRET_KEY)))
                .header("Building-id", "seryoga-zdanie")
                .header("Game-id", gameId.toString())
                .POST(HttpRequest.BodyPublishers.ofString(JsonReader.toJson(
                        BuildInfo.builder()
                                .vector3(new Vector3())
                                .buildingId(buildingData.getId())
                )))
                .uri(URI.create("http://localhost:8000/building"))
                .build();
        HttpClient httpClient = HttpClient.newHttpClient();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(httpResponse.body());
        } catch (InterruptedException e) {
            System.out.println(e.getLocalizedMessage());
        }

        entityManager.close();
    }
}
