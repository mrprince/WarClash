package model.effectors;

import engine.Scene;
import engine.math.Mathf;
import model.entity.UnitData;
import model.units.AttackInfo;
import model.units.Unit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

public class UnitEffectorsTest {
    private static UnitData testUnitData = new UnitData();
    static {
        testUnitData.setArmor(0);
        testUnitData.setArmorType(UnitData.ArmorType.HEAVY);
        testUnitData.setAttack(20);
        testUnitData.setAttackType(UnitData.AttackType.MELEE);
        testUnitData.setAttackSpeed(1f);
        testUnitData.setAttackRange(1f);
        testUnitData.setHp(400);
        testUnitData.setHpRegen(20f);
        testUnitData.setMp(100);
        testUnitData.setMpRegen(5f);
        testUnitData.setMoveSpeed(1f);
        testUnitData.setUnitSize(1f);
        testUnitData.setUnitAbilities(new HashSet<>());
    }

    @Test
    public void testUnitArmorEffector() {
        var scene = new Scene();
        var go = scene.instantiate("matya");
        var unit = go.addComponent(Unit.class);
        unit.init(testUnitData, null);

        // TEST ARMOR TYPES
        unit.defend(new AttackInfo(50, UnitData.AttackType.MELEE, unit, null));
        Assertions.assertEquals(360, unit.getHp().getValue(), Mathf.FLOAT_ERROR);

        unit.defend(new AttackInfo(50, UnitData.AttackType.RANGED, unit, null));
        Assertions.assertEquals(310, unit.getHp().getValue(), Mathf.FLOAT_ERROR);

        unit.defend(new AttackInfo(50, UnitData.AttackType.MAGIC, unit, null));
        Assertions.assertEquals(247.5f, unit.getHp().getValue(), Mathf.FLOAT_ERROR);

        unit.getHp().setValue(400f);

        // TEST ARMOR MODIFIER
        unit.getArmor().setValue(5f);

        unit.defend(new AttackInfo(50, UnitData.AttackType.MELEE, unit, null));
        Assertions.assertEquals(367.38509201f, unit.getHp().getValue(), Mathf.FLOAT_ERROR);
    }

    @Test
    public void testHPRegenEffector() {
        var scene = new Scene();
        var go = scene.instantiate("matya");
        var unit = go.addComponent(Unit.class);
        unit.init(testUnitData, null);

        unit.getHp().changeValue(-10f);
        go.update();
        Assertions.assertEquals(391f, unit.getHp().getValue(), Mathf.FLOAT_ERROR);
    }

    @Test
    public void testMPRegenEffector() {
        var scene = new Scene();
        var go = scene.instantiate("matya");
        var unit = go.addComponent(Unit.class);
        unit.init(testUnitData, null);

        unit.getMp().changeValue(-10f);
        go.update();
        Assertions.assertEquals(90.25f, unit.getMp().getValue(), Mathf.FLOAT_ERROR);
    }
}
