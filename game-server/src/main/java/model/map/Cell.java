package model.map;

import engine.math.Vector3;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Cell {
    public enum CellType {
        BUILDABLE, WALKABLE, INACCESSIBLE
    }

    private final CellType cellType;
    private final Vector3 position;
}
