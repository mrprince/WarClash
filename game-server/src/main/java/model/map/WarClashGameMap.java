package model.map;

import engine.math.Vector3;

import java.util.ArrayList;
import java.util.List;

public class WarClashGameMap extends GameMap<WarClashCell> {
    public WarClashGameMap(float cellSize, Vector3 mapSize, Cell.CellType[] cellTypes) {
        super(cellTypes, mapSize, cellSize);
    }

    @Override
    public void init(Cell.CellType[] cellTypes, Vector3 mapSize) {
        for(int i = 0; i < mapSize.getX(); i++) {
            for(int j = 0; j < mapSize.getY(); j++) {
                for(int k = 0; k < mapSize.getZ(); k++) {
                    var index = new Vector3(i, j, k);
                    int cellTypeIndex = getIndex(i, j, k, mapSize);

                    Cell.CellType cellType;
                    if(cellTypes != null && cellTypes.length > cellTypeIndex && cellTypeIndex >= 0) {
                        cellType = cellTypes[cellTypeIndex];
                    } else {
                        cellType = Cell.CellType.INACCESSIBLE;
                    }

                    getCells().put(new Vector3(i, j, k), new WarClashCell(cellType, index));
                }
            }
        }
    }

    private int getIndex(int i, int j, int k, Vector3 mapSize) {
        return k * (int)mapSize.getX() * (int)mapSize.getY() + j * (int)mapSize.getX() + i;
    }

    public static GameMap<WarClashCell> parseFromString(List<Character> s, float cellSize) {
        List<Cell.CellType> cellTypes = new ArrayList<>();
        int x = 0, y = 0;

        for(int i = 0; i < s.size(); i++) {
            var c = s.get(i);
            if(c == '\n') {
                if(x == 0) {
                    x = i;
                    y = (s.size() + 1) / (x + 1);
                }
                continue;
            }
            switch (c) {
                case 'B': {
                    cellTypes.add(Cell.CellType.BUILDABLE);
                    break;
                }
                case 'W': {
                    cellTypes.add(Cell.CellType.WALKABLE);
                    break;
                }
                default: {
                    cellTypes.add(Cell.CellType.INACCESSIBLE);
                    break;
                }
            }
        }

        return new WarClashGameMap(cellSize, new Vector3(x, y, 1), cellTypes.toArray(new Cell.CellType[0]));
    }
}
