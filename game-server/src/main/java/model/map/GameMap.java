package model.map;

import engine.math.Vector3;
import lombok.Getter;

import java.util.HashMap;

@Getter
public abstract class GameMap<T extends Cell> {
    private final HashMap<Vector3, T> cells;
    private final Vector3 offset;
    private final float scale;

    public GameMap(Cell.CellType[] cellTypes, Vector3 mapSize, float cellSize) {
        cells = new HashMap<>();
        offset = new Vector3(0,0,0);
        scale = cellSize;
        init(cellTypes, mapSize);
    }

    public abstract void init(Cell.CellType[] cellTypes, Vector3 mapSize);

    public T getCell(Vector3 cell) {
        return cells.get(cell);
    }

    public Vector3 fromMap(Vector3 map) {
        return new Vector3(map).mul(scale).add(offset);
    }

    public Vector3 fromSpace(Vector3 space) {
        return new Vector3(space).sub(offset).div(scale).round();
    }
}
