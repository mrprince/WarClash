package model.map;

import engine.math.Vector3;
import lombok.Getter;
import model.Building;

@Getter
public class WarClashCell extends Cell {
    private Building building;

    public WarClashCell(CellType cellType, Vector3 position) {
        super(cellType, position);
    }

    public void addBuilding(Building building) {
        this.building = building;
    }

    public void removeBuilding() {
        this.building = null;
    }
}
