package model;

import engine.GameTaskInfo;
import lombok.Getter;
import model.map.GameMap;
import model.map.WarClashCell;
import model.map.WarClashGameMap;

import java.util.List;

@Getter
public class WarClashGameTaskInfo extends GameTaskInfo {
    private GameMap<WarClashCell> gameMap;

    public WarClashGameTaskInfo(GameTaskInfo gameTaskInfo, List<Character> gameMap) {
        this(gameTaskInfo, WarClashGameMap.parseFromString(gameMap, 1));
    }

    public WarClashGameTaskInfo(GameTaskInfo gameTaskInfo, GameMap<WarClashCell> gameMap) {
        super(gameTaskInfo);
        this.gameMap = gameMap;
    }
}
