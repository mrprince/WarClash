package model.pathfinding;

import engine.Component;
import engine.math.Vector3;
import lombok.Getter;
import lombok.Setter;
import model.effectible.EffectibleFloat;
import model.map.Cell;
import model.map.GameMap;

import java.util.*;

public class Pathfinder<T extends Cell> extends Component {
    @Getter @Setter private GameMap<T> gameMap;
    @Getter @Setter private EffectibleFloat distanceError;

    public Path<T> findPath(Vector3 startPosition, Vector3 endPosition) {
        startPosition = gameMap.fromSpace(startPosition);
        endPosition = gameMap.fromSpace(endPosition);

        Set<Vector3> U = new HashSet<>();
        Map<Vector3, Float> g = new HashMap<>();
        Map<Vector3, Float> f = new HashMap<>();
        Map<Vector3, Vector3> parent = new HashMap<>();

        g.put(startPosition, 0f);
        f.put(startPosition, g.get(startPosition) + Vector3.distance(startPosition, endPosition));

        PriorityQueue<Vector3> frontier = new PriorityQueue<>(11,
                (o1, o2) -> Float.compare(f.get(o1), f.get(o2)));
        frontier.add(startPosition);

        while (!frontier.isEmpty()) {
            Vector3 current = frontier.poll();
            if (current.equals(endPosition)) {
                break;
            }
            U.add(current);
            for (Map.Entry<Vector3, Float> adjacentCell : adjacentCells(current).entrySet()) {
                float tentativeScore = g.get(current) + adjacentCell.getValue();
                if (U.contains(adjacentCell.getKey()) && tentativeScore >= g.get(adjacentCell.getKey())) continue;
                if (!U.contains(adjacentCell.getKey())) {
                    if (!g.containsKey(adjacentCell.getKey()) || tentativeScore < g.get(adjacentCell.getKey())) {
                        parent.put(adjacentCell.getKey(), current);
                        g.put(adjacentCell.getKey(), tentativeScore);
                        f.put(adjacentCell.getKey(), tentativeScore +
                                Vector3.distance(adjacentCell.getKey(), endPosition));
                        if (!frontier.contains(adjacentCell.getKey())) frontier.add(adjacentCell.getKey());
                    }
                }
            }
        }

        List<Vector3> resultList = new ArrayList<>();
        for (Vector3 cell = endPosition; cell != startPosition; cell = parent.get(cell)) resultList.add(cell);

        Collections.reverse(resultList);
        return new Path<>(gameMap, resultList, distanceError);
    }

    private Map<Vector3, Float> adjacentCells(Vector3 cell) {
        Map<Vector3, Float> map = new HashMap<>();

        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i != 0 || j != 0) {
                    Vector3 adjacentCell = cell.add(new Vector3(i, j, 0));
                    if (gameMap.getCell(adjacentCell).getCellType() == Cell.CellType.WALKABLE) {
                        map.put(adjacentCell, (float)Math.sqrt(Math.abs(i) + Math.abs(j)));
                    }
                }
            }
        }

        return map;
    }


}
