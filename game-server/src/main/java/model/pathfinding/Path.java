package model.pathfinding;

import engine.math.Vector3;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import model.effectible.EffectibleFloat;
import model.map.Cell;
import model.map.GameMap;

import java.util.List;

@RequiredArgsConstructor @Getter
public class Path<T extends Cell> {
    public static class CellNotFoundInPathException extends RuntimeException { }

    private final GameMap<T> gameMap;
    private final List<Vector3> path;
    private final EffectibleFloat distanceError;
    private int currentCellIndex;

    public Vector3 getDirection(Vector3 worldPosition) {
        var cellPosition = gameMap.fromMap(path.get(currentCellIndex));
        if(Vector3.distance(cellPosition, worldPosition) > distanceError.getValue() / 2 || isEnded()) {
            return cellPosition.sub(worldPosition).normalized();
        } else {
            currentCellIndex++;
            return getDirection(worldPosition);
        }
    }

    public boolean isEnded() {
        return currentCellIndex == path.size() - 1;
    }

    public Vector3 getNextCell(Vector3 currentCell){
        int index = path.indexOf(currentCell);
        if (index == -1) throw new CellNotFoundInPathException();
        if (index == path.size() + 1) {
            return currentCell;
        } else {
            return path.get(index + 1);
        }
    }
}
