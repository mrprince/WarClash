package model.effectible;

import engine.event.FunctionalEvent;
import lombok.Getter;

public abstract class Effectible<T> {
    protected T value;
    @Getter protected FunctionalEvent<T> getPreprocessors, changePreprocessors, setPreprocessors;

    public Effectible(T defaultValue) {
        value = defaultValue;
        getPreprocessors = new FunctionalEvent<>();
        setPreprocessors = new FunctionalEvent<>();
        changePreprocessors = new FunctionalEvent<>();
    }

    public abstract T getValue();

    public abstract void changeValue(T delta);

    public abstract void setValue(T newValue);
}
