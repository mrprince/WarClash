package model.effectible;

public class EffectibleFloat extends Effectible<Float> {
    public EffectibleFloat(float defaultValue) {
        super(defaultValue);
    }

    @Override
    public Float getValue() {
        return getPreprocessors.invoke(value);
    }

    @Override
    public void changeValue(Float delta) {
        value += changePreprocessors.invoke(delta);
    }

    @Override
    public void setValue(Float newValue) {
        value = setPreprocessors.invoke(newValue);
    }
}
