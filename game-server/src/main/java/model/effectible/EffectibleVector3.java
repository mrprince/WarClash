package model.effectible;

import engine.math.Vector3;

public class EffectibleVector3 extends Effectible<Vector3> {
    public EffectibleVector3(Vector3 defaultValue) {
        super(defaultValue);
    }

    @Override
    public Vector3 getValue() {
        return getPreprocessors.invoke(value);
    }

    @Override
    public void changeValue(Vector3 delta) {
        value.add(changePreprocessors.invoke(delta));
    }

    @Override
    public void setValue(Vector3 newValue) {
        value = setPreprocessors.invoke(newValue);
    }
}
