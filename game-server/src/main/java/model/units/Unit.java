package model.units;

import engine.Component;
import engine.Engine;
import engine.event.Event;
import engine.event.FunctionalEvent;
import engine.event.GenericEvent;
import engine.event.Listener;
import engine.math.Vector3;
import engine.physics.CircleTrigger;
import engine.physics.RigidBody;
import model.effectible.EffectibleFloat;
import model.effectible.EffectibleVector3;
import model.effectors.*;
import model.entity.UnitData;
import com.google.gson.Gson;
import lombok.AccessLevel;
import lombok.Getter;
import model.map.GameMap;
import model.map.WarClashCell;
import model.pathfinding.Path;
import model.pathfinding.WarClashPathfinder;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Unit extends Component {
    public static final float VISION_RANGE = 10;
    public static final float PATH_FIND_DELAY = 0.5f;

    @Getter(AccessLevel.NONE) private static final Gson gson = new Gson();

    // UNIT PROPERTIES START
    private Event onDestroyEvent = new Event();

    // UNIT STATS
    private EffectibleFloat attack, attackSpeed, attackRange;
    private UnitData.AttackType attackType;

    private EffectibleFloat armor;
    private UnitData.ArmorType armorType;

    private EffectibleFloat moveSpeed;
    private EffectibleVector3 moveDirection;

    private EffectibleFloat hp, hpRegen, maxHp;

    private EffectibleFloat mp, mpRegen, maxMp;

    private float unitSize;

    // ATTACK AND DEFEND CALLBACKS
    private FunctionalEvent<AttackInfo> attackPreprocessors = new FunctionalEvent<>(),
            defendPreprocessors = new FunctionalEvent<>();

    private GenericEvent<AttackInfo> onAttackEvent = new GenericEvent<>(), onDefendEvent = new GenericEvent<>();

    // GAME THINGS
    private List<Unit> currentEnemies = new ArrayList<>();
    @Getter(AccessLevel.NONE) private List<Listener> currentEnemyNullers = new ArrayList<>();

    // ENGINE THINGS
    private CircleTrigger visionTrigger, visibilityTrigger, bodyTrigger;
    private RigidBody visionRigidBody, rigidBody;
    private WarClashPathfinder pathfinder;

    private Path<WarClashCell> path;
    private float pathFindTime;

    @Getter(AccessLevel.NONE) private List<Effector> effectors = new ArrayList<>();
    // UNIT PROPERTIES END



    // INITIALIZERS START
    public void init(UnitData data, GameMap<WarClashCell> gameMap) {
        attack = new EffectibleFloat(data.getAttack());
        attackSpeed = new EffectibleFloat(data.getAttackSpeed());
        attackRange = new EffectibleFloat(data.getAttackRange());
        attackType = data.getAttackType();

        armor = new EffectibleFloat(data.getArmor());
        armorType = data.getArmorType();

        moveSpeed = new EffectibleFloat(data.getMoveSpeed());
        moveDirection = new EffectibleVector3(Vector3.zero);

        hp = new EffectibleFloat(data.getHp());
        hpRegen = new EffectibleFloat(data.getHpRegen());
        maxHp = new EffectibleFloat(data.getHp());

        mp = new EffectibleFloat(data.getMp());
        mpRegen = new EffectibleFloat(data.getMpRegen());
        maxMp = new EffectibleFloat(data.getMp());

        unitSize = data.getUnitSize();

        initializeBaseComponents(gameMap);
        initializeVisionGameObject();
        initializeBaseEffectors();
        initializeSpecialEffectors(data);
    }

    private void initializeBaseEffectors() {
        addEffector(new ArmorEffector());
        addEffector(new HealthPointRegenerationEffector());
        addEffector(new ManaPointRegenerationEffector());
        if (attackType == UnitData.AttackType.MELEE) {
            addEffector(new MeleeAttackEffector());
        } else {
            addEffector(new RangeAttackEffector());
        }
    }

    private void initializeSpecialEffectors(UnitData data) {
        for(var ability : data.getUnitAbilities()) {
            try {
                var effector = gson.fromJson(ability.getAbilityStats(), Class.forName(ability.getAbility().getClassName()));
                addEffector((Effector) effector);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void initializeBaseComponents(GameMap<WarClashCell> gameMap) {
        bodyTrigger = getGameObject().addComponent(CircleTrigger.class);
        bodyTrigger.setRadius(unitSize);
        rigidBody = getGameObject().addComponent(RigidBody.class);
        pathfinder = getGameObject().addComponent(WarClashPathfinder.class);
        pathfinder.setGameMap(gameMap);
        pathfinder.setDistanceError(attackRange);
    }

    private void initializeVisionGameObject() {
        var visionGameObject = getGameObject().getScene().instantiate(getGameObject().getName() + "_VISION");
        visionTrigger = visionGameObject.addComponent(CircleTrigger.class);
        visionTrigger.setRadius(VISION_RANGE);

        visionRigidBody = visionGameObject.addComponent(RigidBody.class);
        visionRigidBody.getOnTriggerEnter().getListeners().add(rb -> {
            var visibility = rb.getGameObject().getComponent(VisibilityData.class);
            if(visibility != null) {
                var enemy = visibility.getUnit();
                if(enemy != null) {
                    currentEnemies.add(enemy);
                    currentEnemyNullers.add(() -> currentEnemies.remove(enemy));
                    enemy.onDestroyEvent.getListeners().add(currentEnemyNullers.get(currentEnemies.indexOf(enemy)));
                }
            }
        });
        visionRigidBody.getOnTriggerExit().getListeners().add(rb -> {
            var visibility = rb.getGameObject().getComponent(VisibilityData.class);
            if(visibility != null) {
                var enemy = visibility.getUnit();
                if(enemy != null && currentEnemies.contains(enemy)) {
                    enemy.onDestroyEvent.getListeners().remove(currentEnemyNullers.get(currentEnemies.indexOf(enemy)));
                    currentEnemyNullers.get(currentEnemies.indexOf(enemy)).invoke();
                }
            }
        });

        var visibilityGameObject = getGameObject().getScene().instantiate(getGameObject().getName() + "_VISIBILITY");
        visibilityTrigger = visibilityGameObject.addComponent(CircleTrigger.class);
        visibilityTrigger.setRadius(unitSize);
        visibilityGameObject.addComponent(VisibilityData.class).initialize(visibilityTrigger, this);
    }
    // INITIALIZERS END



    // WORK WITH EFFECTORS START
    public void addEffector(Effector effector) {
        effectors.add(effector);
        effector.onAdd(this);
    }

    public void removeEffector(Effector effector) {
        effectors.remove(effector);
        effector.onDestroy();
    }
    // WORK WITH EFFECTORS END



    // ATTACK AND DEFEND START
    public void attack(Unit enemy) {
        var attackInfo = attackPreprocessors.invoke(new AttackInfo(attack.getValue(), attackType, enemy,this));
        onAttackEvent.invoke(attackInfo);
    }

    public void defend(AttackInfo attackInfo) {
        attackInfo = defendPreprocessors.invoke(attackInfo);
        hp.changeValue(-attackInfo.getDamage());
        onDefendEvent.invoke(attackInfo);
    }
    // ATTACK AND DEFEND END



    // UNIT COMPONENT BEHAVIOUR START
    @Override
    public void update() {
        updateEffectors();

        for (var enemy : currentEnemies) {
            if (Vector3.distance(getGameObject().getTransform().getGlobalPosition(),
                    enemy.getGameObject().getTransform().getGlobalPosition()) -
                    unitSize - enemy.unitSize <= attackRange.getValue()) {
                attack(enemy);
                rigidBody.setVelocity(Vector3.zero);
                return;
            }
        }

        // PATHFINDER PATH FIND PATH AND GO TO NEAREST ENEMY, EITHER GO TO ENEMY'S KING OK??
        Unit nearestEnemy = null;
        for (var enemy : currentEnemies) {
            if (nearestEnemy == null ||
                    Vector3.distance(getGameObject().getTransform().getGlobalPosition(),
                    enemy.getGameObject().getTransform().getGlobalPosition()) <
                    Vector3.distance(getGameObject().getTransform().getGlobalPosition(),
                    nearestEnemy.getGameObject().getTransform().getGlobalPosition())) {
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null) {
            if (pathFindTime <= 0) {
                path = pathfinder.findPath(getGameObject().getTransform().getGlobalPosition(),
                        nearestEnemy.getGameObject().getTransform().getGlobalPosition());
                pathFindTime = PATH_FIND_DELAY;
            }

            var pathDirection = path.getDirection(getGameObject().getTransform().getGlobalPosition());
            if (!path.isEnded()) {
                moveDirection.setValue(pathDirection);
            } else {
                moveDirection.setValue(nearestEnemy.getGameObject().getTransform().getGlobalPosition()
                        .sub(getGameObject().getTransform().getGlobalPosition()).normalized());
            }
        }

        rigidBody.setVelocity(moveDirection.getValue().normalized().mul(moveSpeed.getValue()));
        pathFindTime -= Engine.FIXED_DELTA_TIME;
    }

    @Override
    public void onDestroy() {
        onDestroyEvent.invoke();
        onDestroyEffectors();
    }
    // UNIT COMPONENT BEHAVIOUR END



    // EFFECTORS COMPONENT BEHAVIOUR START
    private void updateEffectors() {
        for(var effector : effectors) {
            effector.update();
        }
    }

    private void onDestroyEffectors() {
        for(var effector : effectors) {
            effector.onDestroy();
        }
    }
    // EFFECTORS COMPONENT BEHAVIOUR END
}
