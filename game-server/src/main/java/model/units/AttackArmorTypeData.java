package model.units;

import model.entity.UnitData;

import java.util.HashMap;
import java.util.Map;

public class AttackArmorTypeData {
    public static final float BAD_ARMOR_DAMAGE = 0.8f, NORMAL_ARMOR_DAMAGE = 1.0f, GOOD_ARMOR_DAMAGE = 1.25f;
    public static final Map<UnitData.ArmorType, Map<UnitData.AttackType, Float>> attackArmorMultiplier = new HashMap<>();

    static {
        Map<UnitData.AttackType, Float> heavy = new HashMap<>(), light = new HashMap<>(), enchanted = new HashMap<>();

        heavy.put(UnitData.AttackType.MELEE, BAD_ARMOR_DAMAGE);
        heavy.put(UnitData.AttackType.RANGED, NORMAL_ARMOR_DAMAGE);
        heavy.put(UnitData.AttackType.MAGIC, GOOD_ARMOR_DAMAGE);
        attackArmorMultiplier.put(UnitData.ArmorType.HEAVY, heavy);

        light.put(UnitData.AttackType.MELEE, GOOD_ARMOR_DAMAGE);
        light.put(UnitData.AttackType.RANGED, BAD_ARMOR_DAMAGE);
        light.put(UnitData.AttackType.MAGIC, NORMAL_ARMOR_DAMAGE);
        attackArmorMultiplier.put(UnitData.ArmorType.LIGHT, light);

        enchanted.put(UnitData.AttackType.MELEE, NORMAL_ARMOR_DAMAGE);
        enchanted.put(UnitData.AttackType.RANGED, GOOD_ARMOR_DAMAGE);
        enchanted.put(UnitData.AttackType.MAGIC, BAD_ARMOR_DAMAGE);
        attackArmorMultiplier.put(UnitData.ArmorType.ENCHANTED, enchanted);
    }
}
