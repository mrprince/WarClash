package model.units;

import model.entity.UnitData;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AttackInfo {
    private float damage;
    private UnitData.AttackType attackType;
    private Unit attacked, attacking;
}
