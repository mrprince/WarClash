package model.units;

import engine.Component;
import engine.physics.CircleTrigger;
import lombok.Getter;

@Getter
public class VisibilityData extends Component {
    private CircleTrigger trigger;
    private Unit unit;

    public void initialize(CircleTrigger trigger, Unit unit) {
        this.trigger = trigger;
        this.unit = unit;
    }
}
