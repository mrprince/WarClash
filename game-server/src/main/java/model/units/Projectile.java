package model.units;

import engine.Component;
import engine.physics.CircleTrigger;
import engine.physics.RigidBody;

public class Projectile extends Component {
    private Unit targetEnemy;
    private float speed;
    private RigidBody rigidBody;

    public void init(AttackInfo attackInfo, float projectileSpeed, float projectileSize) {
        targetEnemy = attackInfo.getAttacked();
        speed = projectileSpeed;

        rigidBody = getGameObject().addComponent(RigidBody.class);
        rigidBody.getOnTriggerEnter().getListeners().add(rb -> {
            var unit = rb.getGameObject().getComponent(Unit.class);
            if(unit == targetEnemy) {
                targetEnemy.defend(attackInfo);
                getGameObject().getScene().destroy(getGameObject());
            }
        });

        var trigger = getGameObject().addComponent(CircleTrigger.class);
        trigger.setRadius(projectileSize);
    }

    @Override
    public void update() {
        super.update();
        rigidBody.setVelocity(targetEnemy.getGameObject().getTransform().getGlobalPosition()
                .sub(getGameObject().getTransform().getGlobalPosition()).normalized().mul(speed));
    }
}
