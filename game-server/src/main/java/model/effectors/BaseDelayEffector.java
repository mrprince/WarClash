package model.effectors;

import com.google.gson.annotations.Expose;
import engine.Engine;

public abstract class BaseDelayEffector extends BaseEffector {
    @Expose protected float time = 0;

    @Override
    public void update() {
        super.update();
        time -= Engine.FIXED_DELTA_TIME;
    }
}
