package model.effectors;

import com.google.gson.annotations.Expose;
import engine.event.FunctionalListener;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import model.units.AttackInfo;
import model.units.Unit;

@Getter(AccessLevel.PROTECTED) @Setter(AccessLevel.PROTECTED)
public abstract class BaseEffector implements Effector {
    @Expose private Unit unit;
    @Expose private FunctionalListener<AttackInfo> effectLambda;

    @Override
    public void onAdd(Unit unit) {
        setUnit(unit);
        start();
    }

    @Override
    public void start() {

    }

    @Override
    public void update() {

    }

    @Override
    public void onDestroy() {

    }
}
