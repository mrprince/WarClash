package model.effectors;

import model.units.Unit;

public class ManaPointRegenerationEffector extends BasePointRegenerationEffector {
    @Override
    public void onAdd(Unit unit) {
        super.onAdd(unit);
        points = unit.getMp();
        regen = unit.getMpRegen();
        max = unit.getMaxMp();
    }
}
