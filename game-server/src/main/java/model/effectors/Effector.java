package model.effectors;

import engine.ComponentBehaviour;
import model.units.Unit;

public interface Effector extends ComponentBehaviour {
    void onAdd(Unit unit);
}
