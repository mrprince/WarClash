package model.effectors;

import com.google.gson.annotations.Expose;
import model.units.Projectile;
import model.units.Unit;

public class RangeAttackEffector extends BaseDelayEffector {
    private static final float PROJECTILE_SPEED = 5f, PROJECTILE_SIZE = 0.2f;

    @Expose private int projectileCounter;

    @Override
    public void onAdd(Unit unit) {
        super.onAdd(unit);
        unit.getOnAttackEvent().getListeners().add(attackInfo -> {
            if(time <= 0) {
                var projectile = getUnit().getGameObject().getScene().instantiate(
                        getUnit().getGameObject().getName() + "_PROJECTILE_" + projectileCounter);
                projectile.addComponent(Projectile.class).init(attackInfo, PROJECTILE_SPEED, PROJECTILE_SIZE);
                projectileCounter++;
                time = 1 / unit.getAttackSpeed().getValue();
            }
        });
    }
}
