package model.effectors;

import model.units.AttackArmorTypeData;
import model.units.AttackInfo;
import model.units.Unit;

public class ArmorEffector extends BaseEffector {
    public static final float ARMOR_EXPONENT = 0.96f;

    public ArmorEffector() {
        setEffectLambda(attackInfo -> new AttackInfo(attackInfo.getDamage() *
                AttackArmorTypeData.attackArmorMultiplier.get(getUnit().getArmorType()).get(attackInfo.getAttackType()) *
                (float)Math.pow(ARMOR_EXPONENT, getUnit().getArmor().getValue()), attackInfo.getAttackType(), attackInfo.getAttacked(), attackInfo.getAttacking()));
    }

    @Override
    public void onAdd(Unit unit) {
        super.onAdd(unit);
        getUnit().getDefendPreprocessors().getListeners().add(getEffectLambda());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getUnit().getDefendPreprocessors().getListeners().remove(getEffectLambda());
    }
}
