package model.effectors;

import engine.Engine;
import model.effectible.EffectibleFloat;
import model.units.Unit;

public class BasePointRegenerationEffector extends BaseEffector {
    protected EffectibleFloat points, regen, max;

    @Override
    public void update() {
        if (points.getValue() < max.getValue()) {
            points.changeValue(regen.getValue() * Engine.FIXED_DELTA_TIME);
        }
    }

    @Override
    public void onAdd(Unit unit) {
        super.onAdd(unit);
    }
}