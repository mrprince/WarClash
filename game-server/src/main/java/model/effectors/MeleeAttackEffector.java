package model.effectors;


import model.units.Unit;

public class MeleeAttackEffector extends BaseDelayEffector {
    @Override
    public void onAdd(Unit unit) {
        super.onAdd(unit);
        unit.getOnAttackEvent().getListeners().add(attackInfo -> {
            if (time <= 0) {
                attackInfo.getAttacked().defend(attackInfo);
                time = 1 / unit.getAttackSpeed().getValue();
            }
        });
    }
}
