package model.effectors;

import model.units.Unit;

public class KingSizeEffector extends BaseEffector {
    @Override
    public void onAdd(Unit unit) {
        super.onAdd(unit);
        getUnit().getVisibilityTrigger().setRadius(Float.MAX_VALUE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getUnit().getVisibilityTrigger().setRadius(getUnit().getUnitSize());
    }
}
