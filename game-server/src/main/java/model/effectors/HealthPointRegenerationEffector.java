package model.effectors;

import model.units.Unit;

public class HealthPointRegenerationEffector extends BasePointRegenerationEffector {
    @Override
    public void onAdd(Unit unit) {
        super.onAdd(unit);
        points = unit.getHp();
        regen = unit.getHpRegen();
        max = unit.getMaxHp();
    }
}
