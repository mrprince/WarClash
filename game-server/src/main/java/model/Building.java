package model;

import engine.Component;
import engine.Engine;
import engine.GameObject;
import engine.math.Vector3;
import model.entity.BuildingData;
import com.google.gson.annotations.Expose;
import lombok.Getter;
import model.map.GameMap;
import model.map.WarClashCell;
import model.units.Unit;

public class Building extends Component {
    public static class IncorrectBuildingUpgradeException extends RuntimeException {}

    @Getter
    private BuildingData buildingData;
    private transient GameMap<WarClashCell> gameMap;
    private float timer;

    @Expose(serialize = false)
    private int counter = 0;

    public void init(BuildingData buildingData, GameMap<WarClashCell> gameMap) {
        this.buildingData = buildingData;
        this.gameMap = gameMap;
    }

    public void spawnUnit() {
        if (buildingData == null || buildingData.getUnitData() == null) {
            return;
        }

        GameObject unitGameObject = this.getGameObject().getScene().instantiate("UNIT_" +
                buildingData.getUnitData().getNameKey().toUpperCase() + "_" + counter++);
        unitGameObject.addComponent(Unit.class).init(buildingData.getUnitData(), gameMap);
        unitGameObject.getTransform().setGlobalPosition(new Vector3(this.getGameObject()
                .getTransform().getGlobalPosition()));
    }

    public void upgrade(BuildingData buildingData) {
        if (!this.buildingData.getUpgrades().contains(buildingData)) throw new IncorrectBuildingUpgradeException();

        this.buildingData = buildingData;
    }

    @Override
    public void start() {

    }

    @Override
    public void update() {
        if (timer < 0) {
            spawnUnit();
            timer = 100;
        }
        timer -= Engine.FIXED_DELTA_TIME;
    }
}
