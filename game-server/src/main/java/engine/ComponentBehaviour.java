package engine;

public interface ComponentBehaviour {
    void start();
    void update();
    void onDestroy();
}
