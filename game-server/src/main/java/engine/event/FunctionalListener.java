package engine.event;

import java.util.EventListener;

public interface FunctionalListener<T> extends EventListener {
    T invoke(T obj);
}
