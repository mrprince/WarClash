package engine.event;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class FunctionalEvent<T> {
    @Getter private List<FunctionalListener<T>> listeners = new ArrayList<>();

    public T invoke(T obj) {
        T result = obj;
        for(var listener : listeners) {
            result = listener.invoke(result);
        }
        return result;
    }
}
