package engine.event;

import java.util.EventListener;

public interface GenericListener<T> extends EventListener {
    void invoke(T obj);
}
