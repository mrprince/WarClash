package engine.event;

import java.util.EventListener;

public interface Listener extends EventListener {
    void invoke();
}
