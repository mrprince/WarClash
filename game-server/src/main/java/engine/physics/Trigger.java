package engine.physics;


import engine.Component;

public abstract class Trigger extends Component {
    public abstract boolean checkCollision(Trigger trigger);
}
