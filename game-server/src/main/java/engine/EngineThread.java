package engine;

import java.util.TimerTask;

public class EngineThread extends TimerTask {
    private final Scene scene;

    public EngineThread(Scene scene) {
        super();
        this.scene = scene;
    }

    @Override
    public void run() {
        for (var gameObject : scene.getGameObjects().values()){
            if (gameObject.isActive()) {
                gameObject.update();
            }
        }
    }
}
