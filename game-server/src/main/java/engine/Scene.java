package engine;

import engine.event.Event;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class Scene {
    public static class NonUniqueGameObjectNameException extends RuntimeException {
        NonUniqueGameObjectNameException(String name) {
            super("Current scene has a gameobject with such name: " + name);
        }
    }

    @Getter private final Map<String, GameObject> gameObjects = new HashMap<>();
    @Getter private final transient Event onExitEvent = new Event();

    public final GameObject instantiate(String name) {
        if (gameObjects.containsKey(name)) {
            throw new NonUniqueGameObjectNameException(name);
        }

        var gameObject = new GameObject(name);
        gameObjects.put(name, gameObject);
        gameObject.setScene(this);
        gameObject.start();
        return gameObject;
    }

    public final void destroy(GameObject gameObject) {
        gameObject.onDestroy();
        gameObjects.remove(gameObject.getName());
    }

    public final void exit() {
        gameObjects.values().forEach(GameObject::onDestroy);
        gameObjects.clear();
        onExitEvent.invoke();
    }
}
