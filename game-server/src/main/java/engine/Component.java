package engine;

import lombok.Getter;
import lombok.Setter;

public abstract class Component implements ComponentBehaviour {
    transient GameObject gameObject;

    @Getter @Setter private boolean enabled = true;

    public void start() {

    }

    public void update() {

    }

    public void onDestroy() {

    }

    public final GameObject getGameObject() {
        return gameObject;
    }
}
