package engine;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Timer;
import java.util.UUID;

@Getter
@Setter(AccessLevel.PRIVATE)
@AllArgsConstructor
public class GameTaskInfo {
    private transient Timer timer;
    private Scene scene;
    private UUID gameId;

    public GameTaskInfo(GameTaskInfo gameTaskInfo) {
        timer = gameTaskInfo.timer;
        scene = gameTaskInfo.scene;
        gameId = gameTaskInfo.gameId;
    }
}
