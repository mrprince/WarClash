package engine.ecs;

import engine.event.Event;

public abstract class Destroyable {
    public Event onDestroy = new Event();

    public final void destroy() {
        onDestroy();
        onDestroy.invoke();
    }

    protected abstract void onDestroy();
}
