package engine.ecs;

import java.util.ArrayList;
import java.util.List;

public class ECSScene implements ECSSystem {
    private final List<ECSEntity> entities = new ArrayList<>();
    private final List<ECSSystem> systems = new ArrayList<>();

    public ECSEntity instantiate() {
        var entity = new ECSEntity(this);
        entities.add(entity);
        return entity;
    }

    public void destroy(ECSEntity entity) {
        if (entity == null || entity.getScene() != this) {
            return;
        }
        entity.destroy();
        entities.remove(entity);
    }

    @Override
    public void start() {
        systems.forEach(ECSSystem::start);
    }

    @Override
    public void update() {
        systems.forEach(ECSSystem::update);
    }
}
