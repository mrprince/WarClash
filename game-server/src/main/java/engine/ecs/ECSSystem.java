package engine.ecs;

public interface ECSSystem {
    void start();
    void update();
}
