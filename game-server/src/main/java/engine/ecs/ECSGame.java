package engine.ecs;

public class ECSGame implements Runnable {
    private final ECSScene scene;

    public ECSGame(ECSScene scene) {
        this.scene = scene;
    }

    public void start() {
        scene.start();
    }

    @Override
    public void run() {
        scene.update();
    }
}
