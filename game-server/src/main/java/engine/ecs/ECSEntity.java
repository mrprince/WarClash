package engine.ecs;

import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ECSEntity extends Destroyable {
    private static class HasSuchComponentException extends RuntimeException {
        HasSuchComponentException(Class<?> componentType) {
            super(componentType.getName());
        }
    }

    private final Map<Class<?>, List<ECSComponent>> components = new HashMap<>();
    private final transient List<ECSComponent> componentList = new ArrayList<>();

    @Getter private final ECSScene scene;

    public ECSEntity(ECSScene scene) {
        this.scene = scene;
    }

    public <T extends ECSComponent> T addComponent(T component) {
        if (getComponent(component.getClass()) != null) {
            throw new HasSuchComponentException(component.getClass());
        }

        Class<?> type = component.getClass();
        while (!type.equals(ECSComponent.class)) {
            var list = components.computeIfAbsent(type, c -> new ArrayList<>());
            list.add(component);
            type = type.getSuperclass();
        }

        componentList.add(component);

        return component;
    }

    @SuppressWarnings("unchecked")
    public <T extends ECSComponent> T getComponent(Class<T> componentType) {
        if (components.get(componentType) != null) {
            return (T) components.get(componentType).get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public <T extends ECSComponent> List<T> getComponents(Class<T> componentType) {
        return (List<T>) components.get(componentType);
    }

    public <T extends ECSComponent> T removeComponent(Class<T> componentType) {
        var component = getComponent(componentType);
        if (component == null) {
            return null;
        }

        Class<?> type = componentType;
        while (!type.equals(ECSComponent.class)) {
            components.get(type).remove(component);
            type = type.getSuperclass();
        }

        componentList.remove(component);

        component.destroy();

        return component;
    }

    @Override
    protected void onDestroy() {
        componentList.forEach(Destroyable::destroy);
    }
}
