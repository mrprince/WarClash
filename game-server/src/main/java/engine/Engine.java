package engine;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.UUID;
import java.util.function.Function;

@Getter
public class Engine<T extends GameTaskInfo> {
    public static final float FIXED_DELTA_TIME = 0.05f;

    private final Map<UUID, T> games = new HashMap<>();

    public void createGame(Function<GameTaskInfo, T> initializer) {
        var uuid = UUID.randomUUID();
        var timer = new Timer();
        var scene = new Scene();
        var baseGameTask = new GameTaskInfo(timer, scene, uuid);

        games.put(uuid, initializer.apply(baseGameTask));
        scene.getOnExitEvent().getListeners().add(() -> {
            timer.cancel();
            games.remove(uuid);
        });

        timer.scheduleAtFixedRate(new EngineThread(scene), 0, (int)(FIXED_DELTA_TIME * 1000));
    }
}
