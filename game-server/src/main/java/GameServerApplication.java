import api.controller.BuildingController;
import api.controller.GameDataController;
import api.controller.SpellCastController;
import api.implementation.BuildingServiceImpl;
import api.implementation.GameDataServiceImpl;
import api.implementation.SpellCastServiceImpl;
import authentication.JWTRESTDecorator;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.sun.net.httpserver.HttpExchange;
import engine.Engine;
import model.Building;
import model.WarClashGameTaskInfo;
import model.entity.BuildingData;
import model.entity.UnitData;
import model.entity.WarClashUserData;
import model.repository.BuildingRepository;
import rest.BaseRESTHandler;
import rest.RESTResponse;
import rest.RESTServer;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class GameServerApplication {

    public static void main(String[] args) throws IOException {
        RESTServer restServer = new RESTServer(8081);

        /*EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("warclash");
        EntityManager entityManager = entityManagerFactory.createEntityManager();*/

        var is = GameServerApplication.class.getClassLoader().getResourceAsStream("test_1.map");
        List<Character> map = new ArrayList<>();
        if(is != null) {
            var reader = new InputStreamReader(is);
            int c;
            while((c = reader.read()) != -1) {
                if(c != '\r') {
                    map.add((char)c);
                }
            }
        }

        var engine = new Engine<WarClashGameTaskInfo>();
        engine.createGame(gameTaskInfo -> {
            var wcg = new WarClashGameTaskInfo(gameTaskInfo, map);

            UnitData unitData = new UnitData();
            unitData.setNameKey("seryoga-zdanie");
            BuildingData buildingData = new BuildingData();
            buildingData.setUnitData(unitData);
            var gm = gameTaskInfo.getScene().instantiate("seryouga");
            var b = gm.addComponent(Building.class);
            b.init(buildingData, wcg.getGameMap());

            return wcg;
        });

        System.out.println(engine.getGames().keySet());

        initBek(restServer);

//        restServer.addEndPoint("/user-data", new JWTRESTDecorator(new UserDataController(new UserDataServiceImpl())));

//        restServer.addEndPoint("/matchmaking", new JWTRESTDecorator(new MatchmakingController(new MatchmakingServiceImpl())));
        restServer.addEndPoint("/game-data", new JWTRESTDecorator(new GameDataController(new GameDataServiceImpl(engine))));
        restServer.addEndPoint("/spell-cast", new JWTRESTDecorator(new SpellCastController(new SpellCastServiceImpl())));
//        restServer.addEndPoint("/card", new JWTRESTDecorator(new CardController(new CardServiceImpl())));
//        restServer.addEndPoint("/match-result", new JWTRESTDecorator(new MatchResultController(new MatchResultServiceImpl())));

        restServer.start();
        System.out.println("Server started");

        System.out.println(JWT.create().withSubject("warclash").withKeyId("111").sign(Algorithm.HMAC256(JWTRESTDecorator.SECRET_KEY)));

        restServer.addEndPoint("/building", new JWTRESTDecorator(new BuildingController(
                new BuildingServiceImpl(
                        engine,
                        new BuildingRepository(null)
                ))));
    }

    private static void initBek(RESTServer restServer) {
        BaseRESTHandler bekRESTHandler = new BaseRESTHandler() {
            @Override
            public RESTResponse handleGET(HttpExchange exchange) {
                return new RESTResponse(200, "GET BEK");
            }

            @Override
            public RESTResponse handlePOST(HttpExchange exchange) {
                return new RESTResponse(200, "POST BEK");
            }

            @Override
            public RESTResponse handlePUT(HttpExchange exchange) {
                return new RESTResponse(200, "PUT BEK");
            }

            @Override
            public RESTResponse handlePATCH(HttpExchange exchange) {
                return new RESTResponse(200, "PATCH BEK");
            }
        };

        restServer.addEndPoint("/bek", new JWTRESTDecorator(bekRESTHandler));
    }

    private static void testEntity(EntityManager entityManager) {
        entityManager.getTransaction().begin();

        WarClashUserData warClashUserData = new WarClashUserData();
        warClashUserData.setName("Novaya Strategiya v Realnom Vremeni)))");
        entityManager.persist(warClashUserData);

        UnitData unitData = new UnitData();
        unitData.setNameKey("seryoga-zdanie");
        entityManager.persist(unitData);

        BuildingData buildingData = new BuildingData();
        buildingData.setUnitData(unitData);
        entityManager.persist(buildingData);

        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
