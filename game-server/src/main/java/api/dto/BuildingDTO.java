package api.dto;

import lombok.Data;

@Data
public class BuildingDTO {

    String buildingName;
}
