package api.controller;

import api.service.SpellCastService;
import com.sun.net.httpserver.HttpExchange;
import lombok.RequiredArgsConstructor;
import rest.BaseRESTHandler;
import rest.RESTResponse;

@RequiredArgsConstructor
public class SpellCastController extends BaseRESTHandler {

    private final SpellCastService spellCastService;

    @Override
    public RESTResponse handlePOST(HttpExchange exchange) {
        return spellCastService.castSpell(exchange);
    }
}
