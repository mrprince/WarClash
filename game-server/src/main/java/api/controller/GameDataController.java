package api.controller;

import api.service.GameDataService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import lombok.RequiredArgsConstructor;
import rest.BaseRESTHandler;
import rest.RESTResponse;

import java.util.UUID;

@RequiredArgsConstructor
public class GameDataController extends BaseRESTHandler {
    private final GameDataService gameDataService;

    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public RESTResponse handleGET(HttpExchange exchange) {
        var gameId = UUID.fromString(exchange.getRequestHeaders().getFirst("Game-id"));
        String s = null;
        try {
            var scene = gameDataService.getGameData(gameId);
            s = gson.toJson(scene);
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
        return new RESTResponse(200, s);
    }
}
