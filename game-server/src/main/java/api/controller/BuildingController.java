package api.controller;

import com.sun.net.httpserver.HttpExchange;
import lombok.RequiredArgsConstructor;
import rest.BaseRESTHandler;
import rest.JsonReader;
import rest.RESTResponse;
import api.info.BuildInfo;
import api.info.CellInfo;
import api.info.UpgradeInfo;
import api.service.BuildingService;

import java.util.UUID;

@RequiredArgsConstructor
public class BuildingController extends BaseRESTHandler {
    private final BuildingService buildingService;

    @Override
    public RESTResponse handlePOST(HttpExchange exchange) {
        var gameId = UUID.fromString(exchange.getRequestHeaders().getFirst("Game-id"));
        var buildInfo = JsonReader.fromJson(exchange.getRequestBody(), BuildInfo.class);
        return new RESTResponse(200, buildingService.build(gameId, buildInfo));
    }

    @Override
    public RESTResponse handlePATCH(HttpExchange exchange) {
        var gameId = UUID.fromString(exchange.getRequestHeaders().getFirst("Game-id"));
        var upgradeInfo = JsonReader.fromJson(exchange.getRequestBody(), UpgradeInfo.class);
        return new RESTResponse(200, buildingService.upgrade(gameId, upgradeInfo));
    }

    @Override
    public RESTResponse handleDELETE(HttpExchange exchange) {
        var gameId = UUID.fromString(exchange.getRequestHeaders().getFirst("Game-id"));
        var cellInfo = JsonReader.fromJson(exchange.getRequestBody(), CellInfo.class);
        return new RESTResponse(200, buildingService.delete(gameId, cellInfo));
    }
}
