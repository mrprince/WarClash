package api.service;

import model.WarClashGameTaskInfo;

import java.util.UUID;

public interface GameDataService {
    WarClashGameTaskInfo getGameData(UUID gameId);
}
