package api.service;

import api.info.BuildInfo;
import api.info.CellInfo;
import api.info.UpgradeInfo;

import java.util.UUID;

public interface BuildingService {
    //Building get(UUID gameId, BuildingDTO buildingDTO);

    String build(UUID gameId, BuildInfo buildInfo);

    String upgrade(UUID gameId, UpgradeInfo upgradeInfo);

    String delete(UUID gameId, CellInfo cellInfo);
}
