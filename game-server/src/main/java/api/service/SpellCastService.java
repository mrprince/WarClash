package api.service;

import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public interface SpellCastService {

    RESTResponse castSpell(HttpExchange exchange);
}
