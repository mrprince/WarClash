package api.info;

import engine.math.Vector3;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class CellInfo {
    private Vector3 vector3;
}
