package api.info;

import lombok.Data;

@Data
public class UpgradeInfo {
    String buildingName;
    int upgradeIndex;
}
