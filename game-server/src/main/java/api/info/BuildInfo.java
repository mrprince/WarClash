package api.info;

import lombok.*;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class BuildInfo extends CellInfo {
    private Long buildingId;
}