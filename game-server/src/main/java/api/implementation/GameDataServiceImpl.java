package api.implementation;

import api.service.GameDataService;
import engine.Engine;
import lombok.RequiredArgsConstructor;
import model.WarClashGameTaskInfo;

import java.util.UUID;

@RequiredArgsConstructor
public class GameDataServiceImpl implements GameDataService {
    private final Engine<WarClashGameTaskInfo> engine;

    @Override
    public WarClashGameTaskInfo getGameData(UUID gameId) {
        return engine.getGames().get(gameId);
    }
}
