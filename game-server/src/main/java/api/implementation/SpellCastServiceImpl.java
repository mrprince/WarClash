package api.implementation;

import api.service.SpellCastService;
import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public class SpellCastServiceImpl implements SpellCastService {
    @Override
    public RESTResponse castSpell(HttpExchange exchange) {
        return new RESTResponse(200, "SPELL CASTED");
    }
}
