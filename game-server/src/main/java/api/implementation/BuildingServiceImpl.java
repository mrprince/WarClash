package api.implementation;

import api.info.UpgradeInfo;
import engine.Engine;
import engine.GameObject;
import model.Building;
import model.WarClashGameTaskInfo;
import model.entity.BuildingData;
import model.repository.BuildingRepository;
import model.repository.CrudRepository;
import lombok.RequiredArgsConstructor;
import api.info.BuildInfo;
import api.info.CellInfo;
import api.service.BuildingService;

import java.util.UUID;

@RequiredArgsConstructor
public class BuildingServiceImpl implements BuildingService {
    private final Engine<WarClashGameTaskInfo> engine;

    private final BuildingRepository buildingRepository;

    /*
    public Building get(UUID gameId, BuildingDTO buildingDTO) {
        Scene scene = engine.getGames().get(gameId).getScene();
        var building = scene.getGameObjects().get(buildingDTO.getBuildingName());
        return building.getComponent(Building.class);
    }
     */

    public String build(UUID gameId, BuildInfo buildInfo) {
        var gameTaskInfo = engine.getGames().get(gameId);

        // need some checks here, like "does player have enough money to build this building?" etc.
        GameObject buildingGameObject = gameTaskInfo.getScene()
                .instantiate("BUILDING_" + buildInfo.getBuildingId() + "_" + "1488");

        buildingGameObject.addComponent(Building.class).init(buildingRepository.read(buildInfo.getBuildingId()), gameTaskInfo.getGameMap());

        buildingGameObject.getTransform().setGlobalPosition(gameTaskInfo.getGameMap().fromMap(buildInfo.getVector3()));

        return buildingGameObject.getName();
    }

    public String upgrade(UUID gameId, UpgradeInfo upgradeInfo) {
        var gameTaskInfo = engine.getGames().get(gameId);

        var building = gameTaskInfo.getScene().getGameObjects().get(upgradeInfo.getBuildingName())
                .getComponent(Building.class);

        // need some checks here, like "does player have enough money to build this building?" etc.
        if (building != null) {
            building.upgrade(building.getBuildingData().getUpgrades().get(upgradeInfo.getUpgradeIndex()));
        }

        return "UPGRADED BUILDING";
    }

    public String delete(UUID gameId, CellInfo cellInfo) {
        var warClashGameTaskInfo = engine.getGames().get(gameId);

        // some building ownership checks here, i guess?

        warClashGameTaskInfo.getScene().destroy(warClashGameTaskInfo.getGameMap()
                .getCell(cellInfo.getVector3()).getBuilding().getGameObject());

        return "DELETED BUILDING";
    }
}