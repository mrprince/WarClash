import api.controller.CardController;
import api.controller.MatchResultController;
import api.controller.MatchmakingController;
import api.controller.UserDataController;
import api.implementation.CardServiceImpl;
import api.implementation.MatchResultServiceImpl;
import api.implementation.MatchmakingServiceImpl;
import api.implementation.UserDataServiceImpl;
import authentication.JWTRESTDecorator;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import rest.RESTServer;

import java.io.IOException;

public class UserServerApplication {
    public static void main(String[] args) throws IOException {
        RESTServer restServer = new RESTServer(8080);

        restServer.addEndPoint("/user-data", new JWTRESTDecorator(new UserDataController(new UserDataServiceImpl())));
        restServer.addEndPoint("/matchmaking", new JWTRESTDecorator(new MatchmakingController(new MatchmakingServiceImpl())));
        restServer.addEndPoint("/card", new JWTRESTDecorator(new CardController(new CardServiceImpl())));
        restServer.addEndPoint("/match-result", new JWTRESTDecorator(new MatchResultController(new MatchResultServiceImpl())));

        restServer.start();
        System.out.println("Server started");

        System.out.println(JWT.create().withSubject("warclash").withKeyId("111").sign(Algorithm.HMAC256(JWTRESTDecorator.SECRET_KEY)));
    }
}
