package api.controller;

import api.service.MatchResultService;
import com.sun.net.httpserver.HttpExchange;
import lombok.RequiredArgsConstructor;
import rest.BaseRESTHandler;
import rest.RESTResponse;

@RequiredArgsConstructor
public class MatchResultController extends BaseRESTHandler {

    private final MatchResultService matchResultService;

    @Override
    public RESTResponse handleGET(HttpExchange exchange) {
        return matchResultService.getMatchResult(exchange);
    }
}
