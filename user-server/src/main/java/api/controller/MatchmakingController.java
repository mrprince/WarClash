package api.controller;

import api.service.MatchmakingService;
import com.sun.net.httpserver.HttpExchange;
import lombok.RequiredArgsConstructor;
import rest.BaseRESTHandler;
import rest.RESTResponse;

@RequiredArgsConstructor
public class MatchmakingController extends BaseRESTHandler {

    private final MatchmakingService matchmakingService;

    @Override
    public RESTResponse handlePOST(HttpExchange exchange) {
        return matchmakingService.findMatch(exchange);
    }

}
