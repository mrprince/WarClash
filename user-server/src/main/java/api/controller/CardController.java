package api.controller;

import api.service.CardService;
import com.sun.net.httpserver.HttpExchange;
import lombok.RequiredArgsConstructor;
import rest.BaseRESTHandler;
import rest.RESTResponse;

@RequiredArgsConstructor
public class CardController extends BaseRESTHandler {

    private final CardService cardService;

    @Override
    public RESTResponse handlePATCH(HttpExchange exchange) {
        return cardService.upgradeCard(exchange);
    }
}
