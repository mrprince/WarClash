package api.controller;

import api.service.UserDataService;
import com.sun.net.httpserver.HttpExchange;
import lombok.RequiredArgsConstructor;
import rest.BaseRESTHandler;
import rest.RESTResponse;

@RequiredArgsConstructor
public class UserDataController extends BaseRESTHandler {

    private final UserDataService userDataService;

    @Override
    public RESTResponse handleGET(HttpExchange exchange) {
        return userDataService.getUserData(exchange);
    }
}
