package api.service;

import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public interface CardService {

    RESTResponse upgradeCard(HttpExchange exchange);
}
