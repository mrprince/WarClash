package api.service;

import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public interface MatchmakingService {

    RESTResponse findMatch(HttpExchange exchange);
}
