package api.service;

import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public interface UserDataService {

    RESTResponse getUserData(HttpExchange exchange);
}
