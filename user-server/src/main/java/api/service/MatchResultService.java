package api.service;

import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public interface MatchResultService {

    RESTResponse getMatchResult(HttpExchange exchange);
}
