package api.implementation;

import api.service.MatchResultService;
import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public class MatchResultServiceImpl implements MatchResultService {
    @Override
    public RESTResponse getMatchResult(HttpExchange exchange) {
        return new RESTResponse(200, "MATCH RESULT");
    }
}
