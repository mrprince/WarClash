package api.implementation;

import api.service.UserDataService;
import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public class UserDataServiceImpl implements UserDataService {
    @Override
    public RESTResponse getUserData(HttpExchange exchange) {
        return new RESTResponse(200, "USER DATA");
    }
}
