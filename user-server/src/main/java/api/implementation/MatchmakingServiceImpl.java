package api.implementation;

import api.service.MatchmakingService;
import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public class MatchmakingServiceImpl implements MatchmakingService {

    @Override
    public RESTResponse findMatch(HttpExchange exchange) {
        return new RESTResponse(200, "FOUND MATCH");
    }
}
