package api.implementation;

import api.service.CardService;
import com.sun.net.httpserver.HttpExchange;
import rest.RESTResponse;

public class CardServiceImpl implements CardService {

    @Override
    public RESTResponse upgradeCard(HttpExchange exchange) {
        return new RESTResponse(200, "CARD UPDATED");
    }
}
