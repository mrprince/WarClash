package rest;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class JsonReader {

    private static final Gson gson = new Gson();

    public static <T> T fromJson(InputStream inputStream, Class<T> tClass) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        return gson.fromJson(reader.lines().collect(Collectors.joining("\n")), tClass);
    }

    public static String toJson(Object object) {
        return gson.toJson(object);
    }
}
