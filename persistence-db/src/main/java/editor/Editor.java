package editor;

import editor.controller.UnitEditorController;
import editor.fielddrawers.*;
import model.entity.UnitData;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Editor extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        var fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("start.fxml"));
        AnchorPane root = fxmlLoader.load();
        var scene = new Scene(root);

        UnitEditorController unitEditorController = fxmlLoader.getController();

        var viewCreator = new ViewCreator();
        viewCreator.initialize(
                new LabelDecoratorPropertyDrawer(new BooleanPropertyDrawer()),
                new LabelDecoratorPropertyDrawer(new EnumPropertyDrawer()),
                new LabelDecoratorPropertyDrawer(new PrimitivePropertyDrawer()),
                new LabelDecoratorPropertyDrawer(new ListPropertyDrawer(viewCreator)),
                new LabelDecoratorPropertyDrawer(new RecursivePropertyDrawer(viewCreator))
        );

        unitEditorController.getFieldsView().setSpacing(8);
        viewCreator.createView(UnitData.class, unitEditorController.getFieldsView());

        stage.setMinWidth(300);
        stage.setMinHeight(200);
        stage.setResizable(true);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}