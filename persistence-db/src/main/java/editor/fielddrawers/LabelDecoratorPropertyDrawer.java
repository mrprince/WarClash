package editor.fielddrawers;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

import java.lang.reflect.Field;

public class LabelDecoratorPropertyDrawer implements PropertyDrawer {
    public static final double defaultLabelWidth = 150;
    private final PropertyDrawer propertyDrawer;

    public LabelDecoratorPropertyDrawer(PropertyDrawer propertyDrawer) {
        this.propertyDrawer = propertyDrawer;
    }

    @Override
    public <T> boolean canDraw(Field field, Class<T> propertyClass) {
        return propertyDrawer.canDraw(field, propertyClass);
    }

    @Override
    public <T> Node draw(Field field, Class<T> propertyClass) {
        var hBox = new HBox();
        var label = new Label(field.getName());

        // hBox.setAlignment(Pos.CENTER_LEFT);
        label.setPrefWidth(defaultLabelWidth);
        label.setFont(Font.font(16));

        hBox.getChildren().add(label);
        hBox.getChildren().add(propertyDrawer.draw(field, propertyClass));

        return hBox;
    }
}
