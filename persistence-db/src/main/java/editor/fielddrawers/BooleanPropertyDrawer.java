package editor.fielddrawers;

import javafx.scene.Node;
import javafx.scene.control.CheckBox;

import java.lang.reflect.Field;

public class BooleanPropertyDrawer implements PropertyDrawer {
    @Override
    public <T> boolean canDraw(Field field, Class<T> propertyClass) {
        return propertyClass.equals(boolean.class) || propertyClass.equals(Boolean.class);
    }

    @Override
    public <T> Node draw(Field field, Class<T> propertyClass) {
        return new CheckBox();
    }
}
