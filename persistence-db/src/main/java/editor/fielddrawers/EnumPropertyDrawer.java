package editor.fielddrawers;

import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;

import java.lang.reflect.Field;

public class EnumPropertyDrawer implements PropertyDrawer {
    @Override
    public <T> boolean canDraw(Field field, Class<T> propertyClass) {
        return propertyClass.isEnum();
    }

    @Override
    public <T> Node draw(Field field, Class<T> propertyClass) {
        var choiceBox = new ChoiceBox<T>();

        choiceBox.getItems().addAll(propertyClass.getEnumConstants());
        choiceBox.setValue(choiceBox.getItems().get(0));

        return choiceBox;
    }
}
