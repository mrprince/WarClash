package editor.fielddrawers;

import editor.ViewCreator;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

import javax.persistence.ManyToOne;
import java.lang.reflect.Field;
import java.util.Arrays;

public class RecursivePropertyDrawer implements PropertyDrawer {
    private final ViewCreator viewCreator;

    public RecursivePropertyDrawer(ViewCreator viewCreator) {
        this.viewCreator = viewCreator;
    }

    @Override
    public <T> boolean canDraw(Field field, Class<T> propertyClass) {
        return Arrays.stream(field.getDeclaredAnnotations()).noneMatch(a -> a.annotationType().equals(ManyToOne.class));
    }

    @Override
    public <T> Node draw(Field field, Class<T> propertyClass) {
        var vbox = new VBox(8);

        viewCreator.createView(propertyClass, vbox);

        return vbox;
    }
}
