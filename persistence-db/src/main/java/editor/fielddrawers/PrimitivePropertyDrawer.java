package editor.fielddrawers;

import javafx.scene.Node;
import javafx.scene.control.TextField;

import java.lang.reflect.Field;
import java.util.*;

public class PrimitivePropertyDrawer implements PropertyDrawer {
    private static final List<Class<?>>
            primitiveIntegers = Arrays.asList(
                    byte.class, char.class, short.class, int.class, long.class,
                    Byte.class, Character.class, Short.class, Integer.class, Long.class),
            primitiveFloats = Arrays.asList(
                    float.class, double.class, Float.class, Double.class
            );



    @Override
    public <T> boolean canDraw(Field field, Class<T> propertyClass) {
        return primitiveIntegers.contains(propertyClass) || primitiveFloats.contains(propertyClass) ||
                propertyClass.equals(String.class);
    }

    @Override
    public <T> Node draw(Field field, Class<T> propertyClass) {
        var input = new TextField();

        input.setPromptText(field.getName());
        input.setOnAction(event -> {
            if (checkCorrectness(field.getType(), input)) {
                input.getStyleClass().remove("error");
            } else {
                input.getStyleClass().add("error");
            }
        });

        return input;
    }

    private <T> boolean checkCorrectness(Class<T> propertyClass, TextField input) {
        if (primitiveIntegers.contains(propertyClass)) {
            return input.getText().matches("\\d*");
        } else if (primitiveFloats.contains(propertyClass)) {
            return input.getText().matches("\\f*");
        }

        return true;
    }
}
