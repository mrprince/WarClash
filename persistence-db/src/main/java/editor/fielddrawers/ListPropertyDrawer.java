package editor.fielddrawers;

import editor.ViewCreator;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;

public class ListPropertyDrawer implements PropertyDrawer {
    private final ViewCreator viewCreator;

    public ListPropertyDrawer(ViewCreator viewCreator) {
        this.viewCreator = viewCreator;
    }

    @Override
    public <T> boolean canDraw(Field field, Class<T> propertyClass) {
        return propertyClass.isArray() || Arrays.asList(propertyClass.getInterfaces()).contains(Collection.class);
    }

    @Override
    public <T> Node draw(Field field, Class<T> propertyClass) {
        var vbox = new VBox(16);
        var type = ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0];

        List<HBox> elements = new ArrayList<>();

        var hbox = new HBox(32);
        var buttonAdd = new Button("Add element");
        var buttonRemove = new Button("Remove element");

        hbox.getChildren().add(buttonAdd);
        hbox.getChildren().add(buttonRemove);
        vbox.getChildren().add(hbox);

        buttonAdd.setOnAction(event -> {
            var element = new HBox(16);
            element.setAlignment(Pos.CENTER_LEFT);

            var index = new Label(elements.size() + ".");
            index.setFont(Font.font(20));
            var vbox2 = new VBox(4);
            element.getChildren().add(index);
            element.getChildren().add(vbox2);

            viewCreator.createView((Class<?>) type, vbox2);

            elements.add(element);
            vbox.getChildren().add(element);
        });

        buttonRemove.setOnAction(event -> {
            if (elements.isEmpty()) {
                return;
            }

            vbox.getChildren().remove(elements.get(elements.size() - 1));
            elements.remove(elements.size() - 1);
        });

        return vbox;
    }
}
