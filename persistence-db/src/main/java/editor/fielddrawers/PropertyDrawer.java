package editor.fielddrawers;

import javafx.scene.Node;

import java.lang.reflect.Field;

public interface PropertyDrawer {
    <T> boolean canDraw(Field field, Class<T> propertyClass);
    <T> Node draw(Field field, Class<T> propertyClass);
}
