package editor;

import editor.fielddrawers.PropertyDrawer;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

public class ViewCreator {
    private final List<PropertyDrawer> propertyDrawers = new ArrayList<>();

    public void initialize(PropertyDrawer... propertyDrawers) {
        this.propertyDrawers.addAll(List.of(propertyDrawers));
    }

    public <T> void createView(Class<T> entityClass, Pane viewParent) {
        for (var field : entityClass.getDeclaredFields()) {
            for (var propertyDrawer : propertyDrawers) {
                if (propertyDrawer.canDraw(field, field.getType())) {
                    viewParent.getChildren().add(propertyDrawer.draw(field, field.getType()));
                    break;
                }
            }
        }
    }
}
