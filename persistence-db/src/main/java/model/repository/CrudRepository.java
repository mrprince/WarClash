package model.repository;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.persistence.EntityManager;

@RequiredArgsConstructor
public abstract class CrudRepository<ValueType, KeyType> {

    @Getter(AccessLevel.PROTECTED)
    private final EntityManager entityManager;

    public void create(ValueType value) {
        entityManager.getTransaction().begin();
        entityManager.persist(value);
        entityManager.getTransaction().commit();
    }

    public abstract ValueType read(KeyType id);

    public void update(ValueType value) {
        create(value);
    }

    public void delete(ValueType value) {
        entityManager.getTransaction().begin();
        entityManager.remove(value);
        entityManager.getTransaction().commit();
    }
}