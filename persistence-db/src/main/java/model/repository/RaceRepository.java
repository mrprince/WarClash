package model.repository;

import model.entity.RaceData;

import javax.persistence.EntityManager;

public class RaceRepository extends CrudRepository<RaceData, Long> {

    public RaceRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public RaceData read(Long id) {
        return getEntityManager().find(RaceData.class, id);
    }
}
