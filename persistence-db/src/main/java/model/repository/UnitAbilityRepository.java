package model.repository;

import model.entity.UnitAbilityData;

import javax.persistence.EntityManager;

public class UnitAbilityRepository extends CrudRepository<UnitAbilityData, Long> {

    public UnitAbilityRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public UnitAbilityData read(Long id) {
        return getEntityManager().find(UnitAbilityData.class, id);
    }
}
