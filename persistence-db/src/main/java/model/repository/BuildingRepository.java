package model.repository;

import model.entity.BuildingData;

import javax.persistence.EntityManager;

public class BuildingRepository extends CrudRepository<BuildingData, Long> {

    public BuildingRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public BuildingData read(Long id) {
        return getEntityManager().find(BuildingData.class, id);
    }
}
