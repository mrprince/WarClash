package model.repository;

import model.entity.UnitData;

import javax.persistence.EntityManager;

public class UnitRepository extends CrudRepository<UnitData, Long> {

    public UnitRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public UnitData read(Long id) {
        return getEntityManager().find(UnitData.class, id);
    }
}
