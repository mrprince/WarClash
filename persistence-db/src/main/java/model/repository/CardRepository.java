package model.repository;

import model.entity.CardData;

import javax.persistence.EntityManager;

public class CardRepository extends CrudRepository<CardData, Long> {

    public CardRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public CardData read(Long id) {
        return getEntityManager().find(CardData.class, id);
    }
}
