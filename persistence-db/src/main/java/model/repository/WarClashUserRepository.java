package model.repository;

import model.entity.WarClashUserData;

import javax.persistence.EntityManager;

public class WarClashUserRepository extends CrudRepository<WarClashUserData, Long> {

    public WarClashUserRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public WarClashUserData read(Long id) {
        return getEntityManager().find(WarClashUserData.class, id);
    }
}
