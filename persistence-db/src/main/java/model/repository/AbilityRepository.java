package model.repository;

import model.entity.AbilityData;

import javax.persistence.EntityManager;

public class AbilityRepository extends CrudRepository<AbilityData, Long> {

    public AbilityRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public AbilityData read(Long id) {
        return getEntityManager().find(AbilityData.class, id);
    }
}
