package model.repository;

import model.entity.WarClashUserCardData;

import javax.persistence.EntityManager;

public class WarClashUserCardRepository extends CrudRepository<WarClashUserCardData, Long> {

    public WarClashUserCardRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public WarClashUserCardData read(Long id) {
        return getEntityManager().find(WarClashUserCardData.class, id);
    }
}
