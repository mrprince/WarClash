package model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "war_clash_user_card")
public class WarClashUserCardData extends IdentifiableEntity {

    @ManyToOne
    private WarClashUserData warClashUser;

    @ManyToOne
    private CardData card;

    private Integer count;

    private Integer level;
}
