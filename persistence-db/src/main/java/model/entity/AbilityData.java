package model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "ability")
public class AbilityData extends IdentifiableEntity{

    public enum TargetType {
        ANY_TARGET, ENEMY, ALLY, NO_TARGET, SELF
    }

    private Boolean aoe;

    @Enumerated(EnumType.ORDINAL)
    private TargetType targetType;

    private String className;
}
