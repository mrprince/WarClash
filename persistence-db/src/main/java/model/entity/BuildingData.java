package model.entity;

import com.sun.istack.Nullable;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "building")
public class BuildingData extends IdentifiableEntity {
    @NaturalId
    String nameKey;

    private Integer cost;

    @OneToOne
    private UnitData unitData;

    @ManyToOne
    @Nullable
    private BuildingData base;

    @OneToMany(mappedBy = "base")
    private List<BuildingData> upgrades;
}
