package model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "unit_ability")
public class UnitAbilityData extends IdentifiableEntity {

    @ManyToOne
    private AbilityData ability;

    @ManyToOne
    private UnitData unit;

    private String nameKey;

    private String descriptionKey;

    /*
    I'm not entirely sure how to actually store this. It should be a json of some kind, I guess, and there are much
     better ways to work with json in Hibernate and Postgres for sure.
     */
    private String abilityStats;
}
