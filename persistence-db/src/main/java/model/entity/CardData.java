package model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "card")
public class CardData extends IdentifiableEntity {

    public enum Rarity {
        COMMON, RARE, EPIC, LEGENDARY
    }

    @Enumerated(EnumType.ORDINAL)
    private Rarity rarity;

    private String descriptionKey;

    private Integer tier;

    @OneToOne
    private UnitData unitData;

    @OneToMany(mappedBy = "card")
    private Set<WarClashUserCardData> warClashUserCard;
}
