package model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "unit")
public class UnitData extends IdentifiableEntity {
    public enum AttackType {
        MELEE, RANGED, MAGIC
    }

    public enum ArmorType {
        HEAVY, LIGHT, ENCHANTED
    }

    private String nameKey;

    @Enumerated(EnumType.ORDINAL)
    private AttackType attackType;

    private int attack;

    private Float attackSpeed;

    private Float attackRange;

    private Integer armor;

    @Enumerated(EnumType.ORDINAL)
    private ArmorType armorType;

    private Float moveSpeed;

    private Integer hp;

    private float hpRegen;

    private Integer mp;

    private float mpRegen;

    private Float unitSize;

    private Boolean bool1;
    private boolean bool2;

    @ManyToOne
    private RaceData race;

    @OneToMany(mappedBy = "unit")
    private Set<UnitAbilityData> unitAbilities;
}