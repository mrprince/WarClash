package model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "race")
public class RaceData extends IdentifiableEntity {

    private String nameKey;

    private String descriptionKey;
}
