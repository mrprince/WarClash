package crud;

import model.entity.RaceData;
import model.repository.CrudRepository;
import model.repository.RaceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CRUDTest {
    @Test
    public void test() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("warclash");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        RaceRepository raceRepository = new RaceRepository(entityManager);

        RaceData race = new RaceData();
        race.setNameKey("Orcs");
        race.setDescriptionKey("Brutal warriors from the other world");
        raceRepository.create(race);

        RaceData testRace = raceRepository.read(race.getId());
        Assertions.assertEquals(testRace.getNameKey(), race.getNameKey());
        Assertions.assertEquals(testRace.getDescriptionKey(), race.getDescriptionKey());

        RaceData newTestRace = new RaceData();
        newTestRace.setNameKey(race.getNameKey());
        newTestRace.setDescriptionKey(race.getDescriptionKey());

        race.setNameKey("Trolls");
        race.setDescriptionKey("Forest hunters with extreme regenerative abilities");

        raceRepository.update(race);
        newTestRace = raceRepository.read(race.getId());
        Assertions.assertEquals(newTestRace.getNameKey(), race.getNameKey());
        Assertions.assertEquals(newTestRace.getDescriptionKey(), race.getDescriptionKey());

        /*raceRepository.delete(race);
        Assertions.assertFalse(entityManager.contains(race));*/
    }
}
